import simra

# ################################################################
# This is not working code yet, it's just here for my own future reference.

def button_press(event):
    print("clicked at ({},{})".format(event.x, event.y))

def button_release(event):
    print("Released at ({},{})".format(event.x, event.y))

def main():
    window = Tk()
    window.title("SIMulateur de Réseau d'Automates")

    canvas = Canvas(window)
    canvas.create_rectangle(128, 64, 32, 32,
        outline="#ff0000", fill="#aaaaaa")
    canvas.bind("<Button-1>", button_press)
    canvas.bind("<ButtonRelease-1>", button_release)
    canvas.pack(fill=BOTH, expand=1)

    window.mainloop()

if __name__ == '__main__':
    main()

