from simra import *

# ################################################################

gm_deps = [
    None, #ignored
    # THESE VALUES ARE 1-indexed instead of 0-indexed. Therefore,
    # Id here == Id in Martìn's schema + 1.
    [3, 5],
    [4, 6],
    [1, 7],
    [2, 7],
    [1, -8],
    [2, -8],
    [3, 4, 9],
    [-5, -6, -9, -10],
    [-8, 13, 14],
    [11, 12],
    [10],
    [10],
    [9, 14],
    [9, 13],
]

gm_init_vals = [
    None, #ignored
    "TODO"
]

gm_update_mode = [
    [],
    "TODO"
]
